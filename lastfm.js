const md5 = require('md5')
const axios = require('axios')

const errors = require('./enums/errors.enum')
require('dotenv').config()

const LASTFM_API = process.env.LASTFM_API
const API_KEY = process.env.API_KEY
const SHARED_SECRET = process.env.SHARED_SECRET

const getTrackData = async (user) => {
  const params = {
    api_key: API_KEY,
    method: 'user.getrecenttracks',
    user: user,
    limit: 1
  }
  console.log('req', params)
  const response = await get(params)
  console.log('response status ' + JSON.stringify(response))
  if (response.status === 404) {
    return { error: errors.UserNotFound }
  }
  if (response.status === 403) {
    return { error: errors.UserPrivate }
  }
  const recentTracks = response.recenttracks
  if (recentTracks === undefined || recentTracks.track[0] === undefined) {
    console.log('no recent tracks')
    return {}
  }
  const lastTrack = map(recentTracks.track[0])
  console.log(lastTrack.trackName)
  return lastTrack
}

const getSession = async (token) => {
  let params = {
    api_key: API_KEY,
    method: 'auth.getSession',
    token: token
  }
  const api_sig = createSignature(params)

  const response = await get({ ...params, api_sig })
  return response.session ? {
    user: response.session.name,
    session: response.session.key
  } : {}
}

const scrobbleTrack = async (track, session) => {
  let params = {
    api_key: API_KEY,
    artist: track.artistName,
    method: 'track.scrobble',
    sk: session,
    timestamp: track.timestamp,
    track: track.trackName
  }
  const api_sig = createSignature(params)
  const response = await post({ ...params, api_sig })
  return response
}

const get = (params) => {
  const url = createGet(params)
  console.log('GET:' + url)
  return new Promise((resolve) => axios
    .get(url)
    .then(response => resolve(response.data))
    .catch(e => {
      console.log(`GET An error occured: ${e.response.status} ${e.response.statusText} @ ${url}`)
      resolve({ status: e.response.status })
    })
  )
}

const post = (params) => {
  const request = createPost(params)
  console.log('POST scrobble:' + request.body)

  return new Promise((resolve) => axios
    .post(request.url, request.body)
    .then(response => {
      resolve(response.status)
    })
    .catch(e => {
      console.log(`POST An error occured: ${e.response.status} ${e.response.statusText} @ ${request.url}`)
      resolve({})
    })
  )
}

const createGet = (params) => {
  let urlParts = []
  const keys = Object.keys(params)
  keys.forEach((key, i) => {
    if (params[key] == undefined)
      throw(`Invalid param: ${key}`)

    let part = `${key}=${params[key]}`
    urlParts.push(part)
  })
  urlParts.push('format=json')
  const queryString = urlParts.join('&')
  return LASTFM_API + '?' + queryString
}

const createPost = (params) => {
  const formBody = Object.keys(params)
    .map(key => encodeURIComponent(key + '=' + params[key]))
    .join('&')
    .replace(/%20/g, '+')
    .replace(/%3D/g, '=')

  return {
    url: `${LASTFM_API}/?format=json`,
    body: formBody
  }
}

const createSignature = (params) => {
  let signature = ''
  const keys = Object.keys(params)
  keys.forEach((key, i) => {
    signature += key + params[key]
  })
  signature += SHARED_SECRET
  return md5(signature)
}

const map = (trackData) => {
  const track = {
    trackName: trackData.name,
    artistName: trackData.artist['#text'],
    imgUrl: trackData.image[3]['#text'],
    exists: true
  }

  if (trackData["@attr"]) {
      track.playing = trackData["@attr"].nowplaying
  }

  if (trackData.date && !trackData.playing) {
    track.timestamp = trackData.date.uts
  } else {
    track.timestamp = Math.floor(Date.now() / 1000)
  }

  return track
}

module.exports = {
    getTrackData,
    getSession,
    scrobbleTrack
}
