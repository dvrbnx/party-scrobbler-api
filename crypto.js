const crypto = require('crypto')
require('dotenv').config()

const SECRET = process.env.SECRET
const ALGORITHM = process.env.ALGORITHM

const encrypt = (text) => {
    console.log('encrypt='+text)
    const iv = crypto.randomBytes(16)
    const cipher = crypto.createCipheriv(ALGORITHM, SECRET, iv)
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()])

    console.log('encrypt IV', iv.toString('hex'))
    console.log('encrypt content', encrypted.toString('hex'))
    return {
        iv: iv.toString('hex'),
        content: encrypted.toString('hex')
    }
}

const decrypt = (iv, content) => {
    console.log('decrypt IV',iv)
    console.log('decrypt content', content)
    const decipher = crypto.createDecipheriv(ALGORITHM, SECRET, Buffer.from(iv, 'hex'))
    const decrypted = Buffer.concat([decipher.update(Buffer.from(content, 'hex')), decipher.final()])

    console.log('decrypted=' + decrypted.toString())
    return decrypted.toString()
}

module.exports = {
    encrypt,
    decrypt
}
