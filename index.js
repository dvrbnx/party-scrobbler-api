const express = require('express')
require('dotenv').config()

const PORT = process.env.PORT || 8000
const cors = require('cors')

const app = express();

// Routing
const indexRouter = require('./routes/index')
const currentTrackRouter = require('./routes/current')
const tokenRouter = require('./routes/token')
const scrobbleRouter = require('./routes/scrobble')

var corsOptions = {
  preflightContinue: true,
  origin: process.env.CLIENT_URL,
  credentials: true,
  optionsSuccessStatus: 200
}


app.use(cors(corsOptions))
app.use(express.json())

// Routes
app.use('/', indexRouter)
   .use('/current', currentTrackRouter)
   .use('/token', tokenRouter)
   .use('/scrobble', scrobbleRouter)

app.listen(PORT)
