const crypto = require('./crypto')
const decrypt = crypto.decrypt

const decryptedSessionCookie = (cookiestring) => {
  if (cookiestring == undefined) {
    return false
  }
  const parts = cookiestring.split('%3B')
  if (parts.length != 2) {
    return false
  }
  return decrypt(parts[0], parts[1])
}

module.exports = {
    decryptedSessionCookie
}
