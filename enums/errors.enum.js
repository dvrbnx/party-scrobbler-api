
const ErrorTypes = {
  General: 'GENERAL',
  UserNotFound: 'USERNOTFOUND',
  UserPrivate: 'USERPRIVATE',
  NotAuthenticated: 'NOAUTH'
}
Object.freeze(ErrorTypes)

module.exports = ErrorTypes
