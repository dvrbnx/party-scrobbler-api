var express = require('express')
var router = express.Router();
const lastFm = require('../lastfm')
const getTrackData = lastFm.getTrackData

const getCurrentTrack = async (req, res, next) => {
  const currentTrack = await getTrackData(req.params.user)
  if (currentTrack.error) {
    return res.status(200).send(currentTrack.error)
  }
  return res.status(200).send(currentTrack)
}

router.get('/:user', getCurrentTrack)

module.exports = router
