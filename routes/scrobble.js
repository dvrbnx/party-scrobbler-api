var express = require('express')
var router = express.Router()
const lastFm = require('../lastfm')
const scrobbleTrack = lastFm.scrobbleTrack
const common = require('../common')

const scrobble = async (req, res, next) => {
  const cookieString = req.headers.cookie;
  const sessionCookie = cookieString.split('=')[1]
  const session = common.decryptedSessionCookie(sessionCookie)
  if (!session) {
    return res.status(304)
  }

  const response = await scrobbleTrack(req.body.track, session)
  return res.status(200).send(response)
}

router.post('/', scrobble)

module.exports = router
