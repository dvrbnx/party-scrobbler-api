var express = require('express')
const router = express.Router()

const crypto = require('../crypto')
const encrypt = crypto.encrypt

const lastFm = require('../lastfm')
const getSession = lastFm.getSession

const handleToken = async (req, res, next) => {
  const lastfmSession = await getSession(req.params.token)
  const session = encrypt(lastfmSession.session)

  return res.cookie('session', `${session.iv};${session.content}`, { httpOnly: true }).status(200).send({
    user: lastfmSession.user
  })
}

router.get('/:token', handleToken)

module.exports = router
